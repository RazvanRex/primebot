import discord
from discord.ext import commands

class Error(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        with open('error.log', 'a') as f:
            f.write('Unhandled Message: {} \n Content: {} \n Error: {}'.format(ctx.message, ctx.message.content, error))
        if isinstance(error, commands.CheckFailure):
            await ctx.send(embed=discord.Embed(color=discord.Color.red(), description=f":x: You don't have the permission to execute this bot command!"))
        if isinstance(error, commands.CommandNotFound):
            await ctx.send(embed=discord.Embed(color=discord.Color.red(), description=f":x: The bot command doesn't exist!"))
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(embed=discord.Embed(color=discord.Color.red(), description=f":x: The command is incomplete, missing one or more parameters!"))
        if isinstance(error, commands.BadArgument):
            await ctx.send(embed=discord.Embed(color=discord.Color.red(), description=f":x: The command was entered incorrectly, one or more parameters are wrong or in the wrong place!"))

def setup(bot):
    bot.add_cog(Error(bot))
