# PrimeBot

A personal bot that runs on Discord.

## How to Run

1. Install dependencies

`make install_dependencies`

2. Create a `.env` file

In the `.env` file, add the following line, `DISCORD_TOKEN="YOUR_TOKEN"`

3. Initialize prefixes

`make initialize`

4. Run the bot

`python bot.py`
